EXEC sp_helpserver -- list linked servers
EXEC sp_helplinkedsrvlogin  -- list linked server logins

-- drop linked server with logins
EXEC sp_dropserver 'sql22', 'droplogins'
-- create a linked server
EXEC sp_addlinkedserver
    @server=N'sql22',
    @srvproduct=N'',
    @provider=N'MSOLEDBSQL',
    @datasrc=N'sql-server-22,1433',
    @catalog='master';

-- specify linked server remote logins
EXEC master.dbo.sp_addlinkedsrvlogin
    @rmtsrvname = N'sql22',
    @locallogin = NULL,
    @useself = N'False',
    @rmtuser = N'sa',
    @rmtpassword = N'sql-server-22@2023'

-- test if linked server is connected
EXEC sp_testlinkedserver sql22;

-- execute query locally to have a base of reference
SELECT * FROM master.sys.databases
-- execute query remotely
Select * from OPENQUERY(sql22, 'SELECT * FROM master.sys.databases')

