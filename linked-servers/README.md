# Linked Servers

This solution tests the use of SQL [linked server]s where one database is able to issue queries to
another SQL server.

```plantuml
@startuml
sql_11 -> sql_11: create linked server
sql_11 -> sql_11: create linked server login
sql_11 -> sql_22: select databases
sql_22 --> sql_11: [master, database_22, ...]
@enduml
```

[linked server]: https://learn.microsoft.com/en-us/sql/relational-databases/linked-servers/create-linked-servers-sql-server-database-engine?view=sql-server-ver16

## Usage

1. `docker compose up`
1. connect to `sql-server-22`
1. create database
1. connect to `sql-server-11`
1. execute `linked-sever.11.sql`

![](./screenshot-2023-06-29 102242.jpg)